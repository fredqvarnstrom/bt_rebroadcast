# Bluetooth re-broadcasting system

## Prepare Raspberry Pi

### Expand file system and update packages.

- Expand file system in `raspi-config`.
- Update packages.
    
        sudo apt-get update
        sudo apt-get upgrade
        
### Install bluez.

    cd ~
    mkdir tmp
    cd tmp  
    wget http://www.kernel.org/pub/linux/bluetooth/bluez-5.37.tar.xz
    tar xvf bluez-5.37.tar.xz
    cd bluez-5.37
    sudo apt-get install -y libusb-dev libdbus-1-dev libglib2.0-dev libudev-dev libical-dev libreadline-dev
    ./configure
    make -j3
    sudo make install
    sudo systemctl enable bluetooth
    

### Working with BT speakers.

- Install dependencies
    
        sudo apt-get --no-install-recommends install pulseaudio pulseaudio-module-bluetooth
    
- If your dongle is a based on a BCM203x chipset, install the firmware.

        sudo apt-get install bluez-firmware
    
- Install MPlayer, along with some codecs, to later test audio output

        sudo apt-get install mplayer
    
- Authorize users (each user that will be using PA must belong to group pulse-access)

        sudo adduser root pulse-access
        sudo adduser pi pulse-access
    
- Authorize PulseAudio - which will run as user pulse - to use BlueZ D-BUS interface:

    Open the config file like this:
    
        sudo nano /etc/dbus-1/system.d/pulseaudio-bluetooth.conf
    
    And add this:
        
        <busconfig>
            <policy user="pulse">
                <allow send_destination="org.bluez"/>
            </policy>
        </busconfig>    
    
- CONFIGURE PULSEAUDIO.

    Open the `/etc/pulse/daemon.conf` and change `resample-method` to anything.
        
    trivial: lowest cpu, low quality
    
    src-sinc-fastest: more cpu, good resampling
    
    speex-fixed-N: N from 1 to 7, lower to higher CPU/quality
        
        resample-method = trivial
    
- Load  Bluetooth discover module in SYSTEM MODE:   

    Open the `/etc/pulse/system.pa` and add this:
    
        .ifexists module-bluetooth-discover.so
        load-module module-bluetooth-discover
        .endif
        
- Create a systemd service for running pulseaudio in System Mode as user `pulse`.
    
    Open the `/etc/systemd/system/pulseaudio.service` and add this:
    
        [Unit]
        Description=Pulse Audio

        [Service]
        Type=simple
        ExecStart=/usr/bin/pulseaudio --system --disallow-exit --disable-shm --exit-idle-time=-1
        
        [Install]
        WantedBy=multi-user.target
    
    Run service.
        
        sudo systemctl daemon-reload
        sudo systemctl enable pulseaudio.service
        
- Connect BT speaker with RPi.
 
- Start PulseAudio - it should pick up the bluetooth devices that it can handle.

        sudo systemctl start pulseaudio.service
        
- Reboot RPi.
        
        sudo reboot 
        
- Test audio mp3

        mplayer -ao pulse file.mp3
      

### Playing music from iPhone.


- Install dependencies.

        sudo apt-get install python-gobject python-gobject-2
        
- Check And Pair Bluetooth.
        
        bluetoothctl:
          scan on
          trust xx:xx:xx:xx:xx:xx(your phone bluetooth mac)
          connect xx:xx:xx:xx:xx:xx(use pi connect phone first, next time you can connect phone with pi on your phone)
    This step will be repeated for 2 BT speakers.

- Config Bluetooth.
    
    Open the `/etc/bluetooth/main.conf` and add the following line in the `[General]` section.
    
        Enable=Source,Sink,Media
    
    Set Bluetooth Class, Modify the following line to:
        
        Class = 0x00041C
    `41C` means your bluetooth contains audio functions
    
- Enable Bluetooth audio.
    
        sudo pactl load-module module-bluetooth-discover
    
    To start it automatically, just open `/etc/rc.local` and add line above.

- connect iPhone with Raspberry Pi.

    Now, you could see your bluetooth sources with:
        
        sudo pactl list sources short
    some texts like:
        
        0       alsa_output.0.analog-stereo.monitor     module-alsa-card.c      s16le 2ch 44100Hz       SUSPENDED
        4       bluez_source.60_F8_1D_9F_0C_AF  module-bluez5-device.c  s16le 2ch 44100Hz       SUSPENDED
    
    `60_F8_1D_9F_0C_AF` is the MAC address of my iPhone.
    
    then show the sinks:
        
        sudo pactl list sinks short
        
    some texts like:
        
        0       alsa_output.0.analog-stereo     module-alsa-card.c      s16le 2ch 44100Hz       SUSPENDED
        1       bluez_sink.11_75_58_5B_D8_AA    module-bluez5-device.c  s16le 2ch 48000Hz       SUSPENDED
        2       bluez_sink.AA_BB_CC_DD_EE_FF    module-bluez5-device.c  s16le 2ch 48000Hz       SUSPENDED
    `11_75_58_5B_D8_AA` and `AA_BB_CC_DD_EE_FF` are the MAC addresses of BT speakers.
    
- Connect all BT components.
        
        $ sudo pactl load-module module-loopback source=bluez_source.60_F8_1D_9F_0C_AF sink=bluez_sink.11_75_58_5B_D8_AA
        $ sudo pactl load-module module-loopback source=bluez_source.60_F8_1D_9F_0C_AF sink=bluez_sink.AA_BB_CC_DD_EE_FF

Now, If you play any music on iPhone, it will be played both on the BT speakers.

- Running python script

Install dependencies.

    sudo pip install pexpect
    sudo apt-get install rfkill
    
Copy source files and run as root.
    
    sudo main.py
    